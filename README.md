# Turpakäräjät: Nenähommat Jouluaattona
![Alt Text](https://gitlab.com/juiffi/turpakarajatjoulu/-/raw/master/anim.gif)  
Olet joulupukki ja tuhmat laps- KURITTOMAT ÄPÄRÄT hyökkäävät kimppuusi! Niillehän annetaan pataan, risujen puutteessa aivan perinteisin menetelmin.  
Nuolet liikkuu ja Z ja X lyö/hyppää.  
  
Kaikki koodi, pl. kirjastot, on roiskaistu yhteen 'turpakarajat.cpp' tiedostoon, ja sillä hyvä. Koodi on myös sopivan vaikealukuista purkkaa. Lisäksi koko SoLoud -kirjasto käännetään, vaikka vain osaa siitä käytetään. Jos kiinnostaisi, niin voisi korjata.

[Lataa Windows versio tästä](https://gitlab.com/juiffi/turpakarajatjoulu/-/blob/master/Release/Release-win.zip)

# Koonti

Komenna
```
git clone https://gitlab.com/juiffi/turpakarajatjoulu  
cd turpakarajatjoulu  
cmake -DCMAKE_BUILD_TYPE=Release -B build/
cd build  
make -j 3
cp -r ../resource/ ./
```

Pitäisi onnistua. Winkkarilla ei pitäs tarvita mitään ylimääräisiä hokkuspokkuksia, jos Visual Studiolla työstää. Linuxilla varmistu, että gcc on versio 8 tai uudempi ja riippuvuudet on asennettu (-lGL -lX11 -lpthread -lpng -ldl).
Ubuntulla esim. näin   
```
sudo apt install libx11-dev libpgn-dev libgl1-mesa-dev  
```

### Koonti Macillä
Joku hurja on saanut kääntymään myös Applen kamppeilla, tarkista ohjeita ao. linkistä ja sovella.  
[https://github.com/MumflrFumperdink/olcPGEMac](https://github.com/MumflrFumperdink/olcPGEMac)

## Pelimoottori julkaistu OLC-3 lisenssillä

* [https://github.com/OneLoneCoder/olcPixelGameEngine](https://github.com/OneLoneCoder/olcPixelGameEngine)
* [https://github.com/OneLoneCoder/olcPixelGameEngine/blob/master/LICENCE.md](https://github.com/OneLoneCoder/olcPixelGameEngine/blob/master/LICENCE.md)

## Santa Claus art by Elthen
* [https://opengameart.org/content/pixel-art-santa-sprites](https://opengameart.org/content/pixel-art-santa-sprites)
* [https://www.patreon.com/elthen](https://www.patreon.com/elthen)

## Enemy sprites by iPixl
* [https://ipixl.itch.io/pixel-art-animated-characters-pack](https://ipixl.itch.io/pixel-art-animated-characters-pack)

## Sounds from freesound
* [https://freesound.org/people/gprosser/sounds/412863/](https://freesound.org/people/gprosser/sounds/412863/)
* [https://freesound.org/people/tommccann/sounds/235968/](https://freesound.org/people/tommccann/sounds/235968/)

## Abby used for collision detection
* [https://github.com/albin-johansson/abby](https://github.com/albin-johansson/abby)

## SoLoud for audio playback
* [https://sol.gfxile.net/soloud/index.html](https://sol.gfxile.net/soloud/index.html)

