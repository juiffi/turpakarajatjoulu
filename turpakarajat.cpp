#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include <cmath>
#include "abby.h"
#include <iterator> // back_inserter
#include <vector>   // vector
#include <iostream> // clog
#include <SoLoud/include/soloud_wav.h>

#define WITH_MINIAUDIO
#include "./SoLoud/include/soloud.h"

class gameObj{
protected:
    int id;
    olc::PixelGameEngine *pge = nullptr;
    olc::Sprite *sprite = nullptr;
    olc::Decal *decal = nullptr;

    bool right = true;
    int frame = 0;
    int attackFrame=0;
    float frameTime = 0.1;
    olc::vf2d pos = {0,0};
    olc::vf2d size = {0,0};
    olc::vf2d hBoxOffset = {0,0};

public:
    olc::vf2d getSize(){return size;}
    olc::vf2d getPos(){return pos;}
    olc::vf2d getHBoxOffset(){return hBoxOffset;}
    int getId(){return id;}
    gameObj(olc::Sprite *sprite,olc::PixelGameEngine *pge){
        this->pge = pge;
        this->sprite = sprite;
        this->decal = new olc::Decal(sprite);
        id = rand();
    }
    virtual void draw(float fElapsedTime){
            pge->DrawPartialDecal(pos,decal,{0,32},{26,24});
    }
    virtual void move(float fElapsedTime){

    }
    virtual bool attack(float fElapsedTime){
        return false;
    }
    bool getDir(){
        return right;
    }

};

class Santa : public gameObj{
private :
    int frame = 0;
    int attackFrame=0;
    float frameTime = 0.1;
    float attackTimer = 0.05;
    float attackTimerMaxTime = 0.04;
    float cooldown=0.25;
    float cooldownMaxTime = 0.25;
    float jumpForce = 0;
    bool running = false;
    bool attacking = false;
    bool attackHand = false;
    bool jumping = false;
    bool whiff=false;
public:
    Santa(olc::Sprite *sprite,olc::PixelGameEngine *pge) : gameObj(sprite,pge){
        pos = {128,202};
        size = {16,28};
        hBoxOffset = {5,4};
    }
    void draw(float fElapsedTime) override{
        frameTime -= fElapsedTime;
        if(frameTime < 0){
            frameTime=0.1;
            frame++;
            if(frame == 8)
                frame=0;
        }
        if (attacking){
            if(attackHand) {
                if (right)
                    pge->DrawPartialDecal(pos, decal, {static_cast<float>(96 * (attackFrame % 5)), 224}, {38, 32});
                else
                    pge->DrawPartialDecal({pos.x-8,pos.y}, decal, {static_cast<float>(96 * (attackFrame % 5)) + 60, 1280}, {38, 32});
            }
            else{
                if (right)
                    pge->DrawPartialDecal(pos, decal, {static_cast<float>(96 * (attackFrame%5)), 320}, {38, 32});
                else
                    pge->DrawPartialDecal({pos.x-8,pos.y}, decal, {static_cast<float>(96 * (attackFrame%5)) + 60, 1376}, {38, 32});
            }
            attackTimer -= fElapsedTime;
            if(attackTimer <0){
                attackFrame++;
                attackTimer= attackTimerMaxTime;
                if(attackFrame==5) {
                    attackFrame = 0;
                    attacking = false;
                }
            }
        }
        else if (running){
                if (right)
                    pge->DrawPartialDecal(pos, decal, {static_cast<float>(96 * frame), 128}, {38, 32});
                else
                    pge->DrawPartialDecal({pos.x-8,pos.y}, decal, {static_cast<float>(96 * frame) + 60, 1184}, {38, 32});

        }
        else{
            if (right)
                pge->DrawPartialDecal(pos, decal, {static_cast<float>(96 * (frame%5)), 32}, {38, 32});
            else
                pge->DrawPartialDecal({pos.x-8,pos.y}, decal, {static_cast<float>(96 * (frame%5)) + 60, 1088}, {38, 32});
        }

        //draw hitbox
        //pge->DrawRect({static_cast<int>(pos.x+5),static_cast<int>(pos.y+4)},{16,28});
/*
        if(attacking && attackFrame >0 && attackFrame <4) {
            if (right) {
                pge->DrawRect({static_cast<int>(pos.x + 24), static_cast<int>(pos.y + 14)}, {10, 8}, olc::RED);
            } else {
                pge->DrawRect({static_cast<int>(pos.x - 7), static_cast<int>(pos.y + 14)}, {10, 8}, olc::RED);
            }
        }
*/
    }

    void move(float fElapsedTime) override{
        running=false;
        if(pge->GetKey(olc::LEFT).bPressed ||pge->GetKey(olc::LEFT).bHeld){
            running =true;
            right=false;
            if(pos.x -(75 * fElapsedTime) > 0 )
            pos.x = pos.x -(75 * fElapsedTime);
            else
                pos.x=0;
        }
        if(pge->GetKey(olc::RIGHT).bPressed ||pge->GetKey(olc::RIGHT).bHeld){
            running =true;
            right=true;
            if(pos.x+( 75 * fElapsedTime) < 256-38)
            pos.x = pos.x +( 75 * fElapsedTime);
            else
                pos.x= 256-38;
        }
        //pos.y = pos.y * gravity * -jumpForce * fElapsedTime;
        if (jumping) {
            pos.y = pos.y-jumpForce*fElapsedTime;
            jumpForce -= fElapsedTime * 500;
        }
        if(pos.y > 202) {
            jumping = false;
            pos.y = 202;
        }
    }
    bool attack(float fElapsedTime) override{
        if(cooldown>0)
        cooldown -= fElapsedTime;
        if(cooldown < 0 && (pge->GetKey(olc::Z).bHeld || pge->GetKey(olc::Z).bPressed)) {
            whiff=false;
            attacking = true;
            cooldown = cooldownMaxTime;
            if(attackHand)
                attackHand=false;
            else
                attackHand=true;

        }
        if(attackFrame>0 && attackFrame <4 && !whiff){
            return true;
        }
        return false;
    }
    void stopAttack(){
        //cooldown=cooldownMaxTime;
        whiff=true;
    }
    void jump(float fElapsedTime){
        if(pos.y >= 202 && (pge->GetKey(olc::X).bPressed || pge->GetKey(olc::X).bHeld)){
            jumping =true;
            jumpForce = 200;
        }
    }
    void fasterAttack(){
            cooldownMaxTime = cooldownMaxTime* 0.9f;
            attackTimerMaxTime = attackTimerMaxTime * 0.9f;


    }
};


class Enemy : public gameObj{
protected:
    Santa *santa = nullptr;
    float frameTime = 0.25;
    int frame = 0;
    bool right;
    float speed = 25;
    int value = 100;
public:
    int getValue(){return value;}
    Enemy(olc::Sprite *sprite, olc::PixelGameEngine *pge, Santa *santa) : gameObj(sprite,pge){
        if((rand()%100) < 50){
            pos.x = -20;
        } else{
            pos.x = 280;
        }
        pos.y=210;
        this->santa =santa;
        size = {14,20 };
        hBoxOffset = {5,0};
    }
    virtual void move(float fElapsedTime) override {
        if(pos.x < santa->getPos().x) {
            pos.x = pos.x + speed * fElapsedTime;
            right =true;
        }
        else{
            pos.x= pos.x - speed*fElapsedTime;
            right = false;
        }
    }

    virtual void draw(float fElapsedTime) override{
        frameTime -= fElapsedTime;
        if(frameTime < 0){
            frameTime=0.1;
            frame++;
            if(frame == 4)
                frame=0;
        }
        pge->SetPixelMode(olc::Pixel::ALPHA);
        if (right)
            pge->DrawPartialSprite(pos, sprite, {(24*frame),32},{26,24});
        else
            pge->DrawPartialSprite(pos, sprite, {(24*frame), 32}, {26,24},1,1);
        pge->SetPixelMode(olc::Pixel::NORMAL);

        //pge->DrawRect(pos.x+hBoxOffset.x,pos.y+hBoxOffset.y,size.x,size.y,olc::RED);
    }
};
class Boy : public Enemy{
protected:
    bool jumping = false;
    float jumpForce = 0;
    float cooldown = 0;
    int jumpThreshold = 0;
    float jumpCharge;
    float maxJumpCharge = 1;
    float shake;
    float maxShake = 0.1;
    bool shakeDir =false;
public:
    Boy(olc::Sprite *sprite, olc::PixelGameEngine *pge, Santa *santa): Enemy(sprite,pge,santa){
        speed = (rand()%10)+30;
        value=200;
        jumpThreshold = (rand()%55)+20;
        jumpCharge = maxJumpCharge;
        shake = maxShake;
    };
    void move(float fElapsedTime) override{
        if(std::abs(pos.x - santa->getPos().x) < jumpThreshold && pos.y ==210 &&cooldown <=0){
            speed = 65;
            jumping = true;
            jumpForce = 150;
            cooldown = 1.5;
        }
        if(pos.y > 210) {
            jumpCharge = maxJumpCharge;
            jumping = false;
            pos.y = 210;
            speed = 35;
        }
        if (jumping) {
            jumpCharge -= fElapsedTime;
            if(jumpCharge <= 0){
            pos.y = pos.y-jumpForce*fElapsedTime;
            jumpForce -= fElapsedTime * 500;
            Enemy::move(fElapsedTime);
            }
            else{
                shake -= fElapsedTime;
                if(shake<=0){
                    shake = maxShake;
                    if(shakeDir)
                        pos.x+=1;
                    else
                        pos.x-=1;
                }
            }
            return;
        }

        if(cooldown >0)
        cooldown -= fElapsedTime;

    Enemy::move(fElapsedTime);
    }
};
class Girl : public Enemy{
public:
    Girl(olc::Sprite *sprite, olc::PixelGameEngine *pge, Santa *santa): Enemy(sprite,pge,santa){
        value=100;
        speed = (rand()%10)+20;
    };
};


class AbbyManager{
private:
    abby::tree<int,float> tree;

public:
    void addToTree(gameObj *obj){
        //in to the tree goes -> pointer to gameobj, min = pos+offset, max = pos+offset+size
        //The line is too long, i know, i know....
        tree.insert(obj->getId(),{obj->getPos().x + obj->getHBoxOffset().x, obj->getPos().y + obj->getHBoxOffset().y}, {obj->getPos().x + obj->getHBoxOffset().x + obj->getSize().x, obj->getPos().y + obj->getHBoxOffset().y + obj->getSize().y});
    }
    void removeFromTree(gameObj *obj){
        tree.erase(obj->getId());
    }
    void updateNode(gameObj *obj){
        tree.update(obj->getId(),{obj->getPos().x + obj->getHBoxOffset().x, obj->getPos().y + obj->getHBoxOffset().y}, {obj->getPos().x + obj->getHBoxOffset().x + obj->getSize().x, obj->getPos().y + obj->getHBoxOffset().y + obj->getSize().y});
    }
    bool checkCollision(gameObj *obj){
        std::vector<int> candidates;
        tree.query(obj->getId(), std::back_inserter(candidates));

        for (const auto candidate : candidates) {
            // Obtains an AABB
            const auto& aabb = tree.get_aabb(candidate);
                if(aabb != tree.get_aabb(obj->getId()) && aabb.overlaps(tree.get_aabb(obj->getId()),true)){
                    //std::cout << "COLLISION DETECTED\n";
                    //std::cout << "A: " << aabb.max().x << ", " << aabb.min().x << "\n";
                    //std::cout << "B: " << tree.get_aabb(obj->getId()).max().x << ", " << tree.get_aabb(obj->getId()).min().x << "\n";
                    return true;
                }
        }
        return false;

    }

    bool addAttack(gameObj *attacker) {
        if(attacker->getDir())
        tree.insert(0,{attacker->getPos().x + 24, attacker->getPos().y+14}, {attacker->getPos().x + 34, attacker->getPos().y +22});
        else
            tree.insert(0,{attacker->getPos().x -7, attacker->getPos().y+14}, {attacker->getPos().x + 3, attacker->getPos().y +22});

        return true;
    }
    bool removeAttack(){
        tree.erase(0);
        return true;
    }
    bool updateAttack(gameObj *attacker){
        if(attacker->getDir())
            tree.update(0,{attacker->getPos().x + 24, attacker->getPos().y+14}, {attacker->getPos().x + 34, attacker->getPos().y +22});
        else
            tree.update(0,{attacker->getPos().x -7, attacker->getPos().y+14}, {attacker->getPos().x + 3, attacker->getPos().y +22});
        return true;
    }
    bool checkAttack(gameObj *attacker){
        std::vector<int> candidates;
        tree.query(0, std::back_inserter(candidates));

        for (const auto candidate : candidates) {
            const auto& aabb = tree.get_aabb(candidate);
            if(aabb != tree.get_aabb(0) && aabb != tree.get_aabb(attacker->getId()) && aabb.overlaps(tree.get_aabb(0),true)){
                hitId=candidate;
                tree.erase(candidate);
                return true;
            }
        }
        return false;
    }
public:
    int hitId=0;
};

class Veri{
public:
    olc::vf2d pos;
    olc::vf2d dir;
    float force;
    float gravity = 350;
    olc::PixelGameEngine *pge=nullptr;
    Veri(olc::vf2d pos,olc::PixelGameEngine *pge){
        this->pos=pos;
        dir = {(float(rand()) / float(RAND_MAX) -0.5f), (float(rand()) / float(RAND_MAX) -1.5f)};
        force = (float(rand())/float(RAND_MAX)) * 4+1;
        this->pge = pge;
    }
    void move(float fElapsedTime){
        if(pos.y <235) {
            pos = pos + dir * force;
            if(force>0)
            force -= fElapsedTime;
            pos.y = pos.y + gravity * fElapsedTime;
        }
    }
    void draw(){
        pge->Draw(pos,olc::RED);
    }
};
class Example : public olc::PixelGameEngine
{
public:
	Example()
	{
		sAppName = "Turpakäräjät: Nenähommat Jouluaattona";
	}
private:
    SoLoud::Soloud* soloud = nullptr;
    SoLoud::Wav* music = nullptr;
    SoLoud::Wav* hit1 = nullptr;
    SoLoud::Wav* explo = nullptr;
    float difficulty=1.1;
    float loadTimer=1;
    olc::Sprite *background = nullptr;
    olc::Sprite *santaSheet = nullptr;
    olc::Sprite *girl_sheet = nullptr;
    olc::Sprite *boy_sheet = nullptr;
    olc::Decal* b_dec = nullptr;
    olc::Decal* g_dec = nullptr;
    Santa *santa =  nullptr;
    Boy *boy = nullptr;
    Girl *girl = nullptr;
    std::vector<Enemy*> *enemies = nullptr;
    int score =0;
    int levelUp= 1000;
    bool startgame=false;
    bool nyrkki=false;
    AbbyManager *abbyManager =nullptr;
    std::vector<Veri*> *veripisarat = nullptr;
    int maxVeri = 5;
    int curveri = 0;

    bool gameover= false;
public:
	bool OnUserCreate() override
	{
        soloud = new SoLoud::Soloud();
        music = new SoLoud::Wav();
        hit1 = new SoLoud::Wav();
        explo = new SoLoud::Wav();
		// Called once at the start, so create things here
        soloud->init();
        music->load("resource/raining2.ogg");
        hit1->load("resource/hit.ogg");
        explo->load("resource/explo.wav");
        music->setLooping(1);
        soloud->play(*music);
		loadTimer = 1;
        difficulty = 1.1;
        score = 0;
        levelUp = 1000;
        nyrkki = false;
        maxVeri = 100;
        curveri = 0;
		background = new olc::Sprite("resource/bg2.png");
		santaSheet = new olc::Sprite("resource/santasheet.png");
		girl_sheet = new olc::Sprite("resource/5_2.png");
		boy_sheet = new olc::Sprite("resource/13_2.png");
        g_dec = new olc::Decal(girl_sheet);
        b_dec = new olc::Decal(boy_sheet);
		santa = new Santa(santaSheet,this);
		girl = new Girl(girl_sheet,this,santa);
		boy = new Boy(boy_sheet,this,santa);
		abbyManager = new AbbyManager();
		startgame = false;
		enemies  = new std::vector<Enemy*>;
		enemies->push_back(girl);
		enemies->push_back(boy);
        enemies->push_back(new Girl(girl_sheet,this,santa));
		veripisarat= new std::vector<Veri*>((maxVeri+1)*1000);
        abbyManager->addToTree(santa);

        for(auto enemy : *enemies){
            abbyManager->addToTree(enemy);
        }
		//std::cout << santa->getId() <<std::endl;
		//std::cout << boy->getId() <<std::endl;
		//std::cout << girl->getId() <<std::endl;
		return true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{

	   //If loadTimer >0, draw noise on screen

	    if(loadTimer >0) {
            loadTimer -= fElapsedTime;
            for (int x = 0; x < ScreenWidth(); x++)
                for (int y = 0; y < ScreenHeight(); y++)
                    Draw(x, y, olc::Pixel(rand() % 255, rand() % 255, rand() % 255));
        }

	    else{
            if(startgame){
                if(!gameover) {
                    //gamecode here
                    Clear(olc::BLACK);
                    //Draw bg image
                    DrawSprite({0, 0}, background);
                    santa->move(fElapsedTime);

                    if (santa->attack(fElapsedTime)) {

                        if (nyrkki) {
                            abbyManager->updateAttack(santa);
                        }
                        if (!nyrkki) {
                            soloud->play(*hit1);
                            nyrkki = abbyManager->addAttack(santa);
                        }
                        if (abbyManager->checkAttack(santa)) {
                            //std::cout << "Turpaan! " << rand() << "\n";
                            santa->stopAttack();
                            Enemy *temp;
                            for (auto enemy : *enemies) {
                                if (enemy->getId() == abbyManager->hitId) {
                                    temp = enemy;
                                    score += temp->getValue();
                                    if (score > 100 && score % levelUp == 0 || score % levelUp == 100) {
                                        levelUp += levelUp;
                                        santa->fasterAttack();
                                    }
                                }
                            }
                            enemies->erase(std::remove(enemies->begin(), enemies->end(), temp), enemies->end());

                            for (int i = 0 + (curveri * 1000); i < 1000 * (curveri + 1); i++) {
                                veripisarat->at(i) = new Veri(temp->getPos() + temp->getHBoxOffset(), this);
                            }

                            curveri++;
                            if (curveri == maxVeri)
                                curveri = 0;

                            delete temp;

                            if (rand() % 10 < 5) {
                                enemies->push_back(new Girl(girl_sheet, this, santa));
                                abbyManager->addToTree(enemies->back());
                            } else {
                                enemies->push_back(new Boy(boy_sheet, this, santa));
                                abbyManager->addToTree(enemies->back());
                            }
                            if (float (rand())/float(RAND_MAX) + 1 < difficulty){
                                if (rand() % 10 < 5) {
                                    enemies->push_back(new Girl(girl_sheet, this, santa));
                                    abbyManager->addToTree(enemies->back());
                                } else {
                                    enemies->push_back(new Boy(boy_sheet, this, santa));
                                    abbyManager->addToTree(enemies->back());
                                }
                            }

                            difficulty += 0.001;
                        }
                    } else {
                        nyrkki = false;
                        abbyManager->removeAttack();
                    }
                    santa->jump(fElapsedTime);
                    santa->draw(fElapsedTime);


                    for (auto enemy : *enemies) {
                        enemy->move(fElapsedTime);
                        enemy->draw(fElapsedTime);
                    }
                    for (int i = 0; i < veripisarat->size(); i++) {
                        if (veripisarat->at(i) != nullptr) {
                            veripisarat->at(i)->move(fElapsedTime);
                            veripisarat->at(i)->draw();
                        }
                    }

                    if(abbyManager->checkCollision(santa)){
                        gameover=true;
                        curveri=0;
                        for(int j=0;j<maxVeri;j++) {
                            for (int i = 0 + (curveri * 1000); i < 1000 * (curveri + 1); i++) {
                                veripisarat->at(i) = new Veri(santa->getPos() + santa->getHBoxOffset(), this);
                            }
                            curveri++;
                        }
                        soloud->play(*explo);
                    }

                    abbyManager->updateNode(santa);
                    for (auto enemy: *enemies) {
                        abbyManager->updateNode(enemy);
                    }
                    std::string s = "Difficulty: ";
                    s.append(std::to_string(difficulty));
                    s.append("\nScore: ");
                    s.append(std::to_string(score));
                    s.append("\nLevel Up at:");
                    s.append(std::to_string(levelUp));
                    DrawStringDecal({20, 10}, s, olc::RED);
                } else{
                    //gamecode here
                    Clear(olc::BLACK);
                    //Draw bg image
                    DrawSprite({0, 0}, background);

                    for (auto enemy : *enemies) {
                        enemy->move(fElapsedTime);
                        enemy->draw(fElapsedTime);
                    }
                    for (int i = 0; i < veripisarat->size(); i++) {
                        if (veripisarat->at(i) != nullptr) {
                            veripisarat->at(i)->move(fElapsedTime);
                            veripisarat->at(i)->draw();
                        }
                    }
                    for (auto enemy: *enemies) {
                        abbyManager->updateNode(enemy);
                    }
                    std::string s = "Difficulty: ";
                    s.append(std::to_string(difficulty));
                    s.append("\nScore: ");
                    s.append(std::to_string(score));
                    s.append("\nLevel Up at:");
                    s.append(std::to_string(levelUp));
                    DrawStringDecal({20, 10}, s, olc::RED);

                    DrawStringDecal({100,100},"KUOLIT",olc::RED,{2.0f,2.0f});

                    if(GetKey(olc::ENTER).bHeld || GetKey(olc::ENTER).bPressed) {
                        gameover=false;
                        soloud->deinit();
                        OnUserCreate();
                        
                    }
                }
            }
            //intro screen
            else{
	        Clear(olc::BLACK);
	        //Draw bg image
            DrawSprite({0,0},background);
            //Draw Text
	        DrawStringDecal({20,10},"Turpakarajat:\n\nNenahommat jouluaattona", olc::RED);
	        Draw({69,10},olc::RED);
	        Draw({74,10},olc::RED);

	        Draw({85,10},olc::RED);
	        Draw({90,10},olc::RED);


	        Draw({101,10},olc::RED);
	        Draw({106,10},olc::RED);


	        Draw({45,26},olc::RED);
	        Draw({50,26},olc::RED);

            DrawStringDecal({20,200}, "Paina Enter", olc::RED);

            for (int i =0;i<2;i++){
                float posY = 40+((i+1)*40);
                std::string s = std::to_string((i+1)*100);
                std::string pointsstring =  "= " +s + " points";
                DrawStringDecal({40,posY},pointsstring, olc::RED);
                DrawStringDecal({40,posY},pointsstring, olc::RED);
                if(i==0)
                DrawPartialDecal({ 15,posY-8 }, g_dec, { (24),32 }, { 26,24 });
                if(i==1)
                DrawPartialDecal({ 15,posY-8 }, b_dec, { (24),32 }, { 26,24 });
            }
            if(GetKey(olc::ENTER).bHeld || GetKey(olc::ENTER).bPressed)
                startgame = true;
            }
	    }
		return true;
	}
};

int main()
{
    srand (time(nullptr));
	Example demo;
	if (demo.Construct(256, 240, 4, 4, false, true))
		demo.Start();

	return 0;
}
